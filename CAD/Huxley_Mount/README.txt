                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1371024
Huxley E3D Lite6 Mount (with GT2 Belt clamp X-carriage) v0.9 by hugofitz is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

I needed a clamp for my new E3D Lite 6 hotend. The original hotend on my second-hand Huxley was pretty hopeless and the X-carriage fell apart/warped.

My initial design has lost about 5mm Z-Axis travel (ie the nozzle is 5mm lower than the stock setup) and moved the head about 7mm further from the X-Axis rods which hasn't really impacted the usable print area on the compact Huxley too much. I've added a heat shield to prevent x carriage warp. This is made from a scrap of coke can cut with scissors to size.

The hole centres on the the X carriage are as standard though I've opened the holes up to accept M3 bolts. I've use washers with nuts as theses speak the load better than an nut trap and are easier to print! 

The X-Cariage is remixed from Weldingrod's design to allow the use of a GT2 belt on my Huxley as I've upgraded to CNC'd 16 tooth pulleys.

Requires 
2of M3 x 40 socket cap bolt
2of M3 x 20 button head bolt
4of M3 nut
4of M3 washer
2of miniature screws (to hold heat sink)
1 scrap of polished coke can to act as heat shield

NOTE this is a work in progress and will update once I've tested it more!

TESTING NOTES SO FAR.........

The fan shroud is set a tan angle so that the fan just clears the X axis motor end when the X carriage is at the limit of it's travel. This still allows excellent airflow through the heat sink. If you get the angle wrong the fan collides with the X Axis motor end before it hits the limit switch when trying to home.

I'm planning to beef up the mount a little to take a bit more flex out on the mounting L plate and add a stop to allow the fan shroud to be aligned so it can't hit the X axis motor mount. I'm trying to keep the moving mass of the X Carriage down as much as possible as I'm planning to add a part cooling fan soon(ish).

Will upload when done.


(ps In case you wondered my printer has been christened Bertha and I'll upload the STL's with out the name on in future!)



# Print Settings

Printer Brand: RepRap
Printer: Huxley
Rafts: Doesn't Matter
Supports: Doesn't Matter
Resolution: 0.2
Infill: 0.4